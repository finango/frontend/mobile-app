# Flutter Frontend

# Connecting to phone from docker
From the host
1. `cd ~/Library/Android/sdk/platform-tools/`
1. `./adb devices`
1. `./adb tcpip 5555`
1. `./adb connect <Phone IP>:5555`
1. `./adb devices`
From the container
1. `adb connect <Phone IP>:5555`
1. `adb devices`

For more info visit [here](https://blog.codemagic.io/how-to-dockerize-flutter-apps/)

# Running development on VS Code
## Web
* In the terminal type `flutter devices` to confirm two devices appear, web-server and the phone
* Type `flutter run -d web-server --web-hostname 0.0.0.0 --web-port=3000`
* Visit localhost:3000
* Press r everytime you want to reload and also refresh webpage
## Phone
* Run `flutter run -d <device-id>` OR
* Hit F5 and select Dart/Flutter
* Progress will appear on the Debug Console on the bottom of the screen
* Wait and the app should load on the phone

# Creating a release
## Web
* Run `flutter build web`
## Phone
### Android
* Run `flutter build appbundle` or `flutter build apk`
### IOS
* Run `flutter build ios` (Mac OS X host only)
Note: Later will need to do [this](https://blog.codemagic.io/how-to-develop-and-distribute-ios-apps-without-mac-with-flutter-codemagic/)
