FROM ubuntu:18.04

# Prerequisites
RUN apt update && apt install -y sudo curl git unzip xz-utils zip libglu1-mesa openjdk-8-jdk wget ruby ruby-dev vim-common
# Prepare Android directories and system variables
RUN mkdir -p Android/sdk
ENV ANDROID_SDK_ROOT /Android/sdk
RUN mkdir -p .android && touch .android/repositories.cfg

# Set up Android SDK
RUN wget -O sdk-tools.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
RUN unzip sdk-tools.zip && rm sdk-tools.zip
RUN mv tools Android/sdk/tools
RUN cd Android/sdk/tools/bin && yes | ./sdkmanager --licenses
RUN cd Android/sdk/tools/bin && ./sdkmanager "build-tools;29.0.2" "patcher;v4" "platform-tools" "platforms;android-29" "sources;android-29"
ENV PATH "$PATH:/Android/sdk/platform-tools"

# Download firebase tools
RUN curl -sL firebase.tools | bash

# Download Flutter SDK
RUN git clone https://github.com/flutter/flutter.git
ENV PATH "$PATH:/flutter/bin"

# Run basic check to download Dark SDK
RUN flutter channel beta
RUN flutter upgrade
RUN flutter config --enable-web
RUN mkdir -p /app
WORKDIR /app
COPY . .
# Fastlane config
RUN apt update && apt install -y libc6-dev g++ build-essential
RUN gem install rake
RUN gem install bundler -v 2.2.0
RUN gem install fastlane -NV
RUN bundle install
RUN bundle update
